<?php

declare(strict_types=1);

namespace App\Presenters;

use UserBraun;
use Nette;

final class MatyasBraunElektronikaPresenter extends Nette\Application\UI\Presenter {

    public function actionMatyasBraunEdit() {
        $this->template->jmeno = "MatyasBraun - Edit";
    }

    public function actionMatyasBraunDelete() {
        $this->template->jmeno = "MatyasBraun - Delete";
    }

    public function actionMatyasBraunAdd() {
        $this->template->jmeno = "MatyasBraun - Add";
    }

}
