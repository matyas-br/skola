<?php 
declare(strict_types=1);

?>
<html>
 <head><title>BraunMatyas</title></head>
 <body>
     
<?php 

/**
 * @author Matyáš Braun
 */


class MatyasBraunTranslateService {
    
    public function translate(string $text) : string
    {
        return "translated ({$text})";
    }
}

class MatyasBraun {
    public $Ahoj = 124; 
    public $NecoKSnidani = 555;
    public $Paraada = 104;
    public $Zmrzlina = 101;
    public $Bramborak = 2;
    public $text = 'HELLO'; 
    public $text2 = '';
    protected $Heslo1 = 264556789734;
    protected $Heslo2 = 44444444444; 
    protected $Heslo3 = 9879789;
    private $OsobniUdaje = 189456486;
    private $Tajemstvi = 644865;

    /**
     * Objekt pro překlady
     * @var MatyasBraunTranslateService
     */
    private $ts;

    public function __construct(MatyasBraunTranslateService $ts)
    {
        $this->ts = $ts;
    }
    
    public function getAhoj () {
       return $this->Ahoj;
    }
    public function getNecoKSnidani () {
       return $this-> NecoKSnidani;
    }
    public function getParaada () {
       return $this-> Paraada;
    }
    public function getZmrzlina () {
       return $this-> Zmrzlina;
    }
    public function getBramborak () {
       return $this-> Bramborak;
    }

    private function setOsobniUdaje () {
       return $this-> OsobniUdaje; 
    }
    private function setTajemství () {
        return $this-> Tajemství;
    }
    public function _construct () {

    }
    
    /**
     * Přelož text.
     * @param string $text
     * @return string
     */
    public function translate (string $text) : string {
        echo "volání fce translate s par. ({$text})<br/>";
        return $this->ts->translate($text);
        //return $text2;
    }
    
    /**
     * Vrátí překládací službu.
     * @return MatyasBraunTranslateService
     */
    public function gettranslate () {
       return $this->ts;
    }
    
    public function getText () {
        echo "volání fce getText<br/>";
        return $this->text;
    }
    
} 

$ts = new MatyasBraunTranslateService();

$aaa = new MatyasBraun($ts);
var_dump($aaa->getAhoj());
var_dump($aaa->getNecoKSnidani());

$instance = new MatyasBraun($ts);
var_dump($instance);

$Ahoj = new MatyasBraun($ts);
$Ahoj->getAhoj('');
echo $Ahoj->getAhoj();
echo '<br>';

$NecoKSnidani = new MatyasBraun($ts);
$NecoKSnidani->getNecoKSnidani('');
echo $NecoKSnidani->getNecoKSnidani();
echo '<br>';

$Paraada = new MatyasBraun($ts);
$Paraada->getParaada('');
echo $Paraada->getParaada();
echo '<br>';

$Zmrzlina = new MatyasBraun($ts);
$Zmrzlina-> getZmrzlina('');
echo $Zmrzlina->getZmrzlina();
echo '<br>';

$Bramborak = new MatyasBraun($ts);
$Bramborak->getBramborak('');
echo $Bramborak->getBramborak();
echo '<br>';

$translate = new MatyasBraun($ts);
$translate->gettranslate('');
echo "<h2>translate:</h2>\n";
echo "<h3>Objekt:</h3>\n";
var_dump($translate->gettranslate());
echo "<h3>Překlad:</h3>\n";
echo "<p>";
echo $translate->translate(
    $translate->getText()
);
echo "</p>\n";


echo (new MatyasBraun($ts))
    ->translate("ABC");

?>

        
    </body>
</html>
